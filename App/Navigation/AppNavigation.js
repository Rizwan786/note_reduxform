import { StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import styles from './Styles/NavigationStyles'
import NoteScreen from '../Containers/NotesScreen'
const PrimaryNav = StackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  NoteScreen: {screen: NoteScreen}

}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
