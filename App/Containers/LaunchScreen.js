import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TextInput, TouchableOpacity, AsyncStorage, Dimensions  } from 'react-native'
import { reduxForm, Field, formValues, formValueSelector} from 'redux-form';
import _ from 'underscore'
import styles from './Styles/LaunchScreenStyles'

const {width, height} = Dimensions.get('window')

class Form extends Component{
  constructor(props){
    super(props);
    this.state = {
      note: []
    }
    this.showAllNotes = this.showAllNotes.bind(this)
    this.showData = this.showData.bind(this)
  }

  componentWillMount(){
    this.showData()
  }

  showData(){
    AsyncStorage.getItem("notes")
      .then((val) => this.setState({note: JSON.parse(val)}))
  }

  showAllNotes(){
    this.props.navigation.navigate('NoteScreen')
  }

  _submit = (values) => {
    let data = []
    data.push(values)
    let allNotes = _.union(data, this.state.note)
    this.setState({note: allNotes})
    AsyncStorage.setItem('notes', JSON.stringify(allNotes))
  }

  render(){
    const {handleSubmit} = this.props
    const renderInput = ({ input: { onChange, ...restInput }}) => {
      return <TextInput
        style={styles.inputField}
        multiline = {true}
        onChangeText={onChange} {...restInput} />
    }

    return(
      <View style={styles.container}>
        <Text style={{fontSize: 16, marginLeft: width * 0.15}}>Note:</Text>
        <Field name="Note" component={renderInput} />
        <View >
          <TouchableOpacity style={styles.btn} onPress={handleSubmit(this._submit)}>
            <Text style={styles.btnText}>Create</Text>
          </TouchableOpacity>
        </View>

        <View >
          <TouchableOpacity style={styles.btn} onPress={()=> this.showAllNotes()}>
            <Text style={styles.btnText}>Show All Notes</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
export default reduxForm({
  form: 'test',
  fields: ['Note']
})(Form)
