import React, {Component} from 'react'
import {Text, View, TouchableOpacity, AsyncStorage, Dimensions, ScrollView, Image} from 'react-native'
import {getFormValues} from 'redux-form'
import {connect} from 'react-redux'
import styles from './Styles/NoteScreenStyle'
import Modal from "react-native-modal";

const {width, height} = Dimensions.get('window')

class NoteScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      note:[],
      toggle: '',
      modalVisible: false,
      currentModal: ''
    }
    this.showData = this.showData.bind(this)
    this.showModal = this.showModal.bind(this)
  }

  componentWillMount(){
   this.showData()
  }

  showData(){
    AsyncStorage.getItem("notes")
      .then((val) => this.setState({note: JSON.parse(val)}))
  }

  showModal(note){
    this.setState({modalVisible: !this.state.modalVisible, currentModal: note})
  }

  render(){
    return(
      <View style={styles.section}>
        <View style={styles.headerView}>
          <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
            <Image style={{width: width* 0.04, height: height* 0.04, marginLeft: width*0.02, marginTop: height* 0.01}} source={require('./../Images/leftArrow.png')}/>
          </TouchableOpacity>
        </View>
      <ScrollView>
        {this.state.note.map((note, index)=>{
          return(
            <TouchableOpacity onPress = {()=> this.showModal(note.Note)}>
              <View style={styles.noteView2}>
                <Text style={{fontSize: 16}}>{note.Note}</Text>
              </View>
            </TouchableOpacity>
          )
        })}
      </ScrollView>
          <Modal
            animationIn = 'slideInUp'
            isVisible={this.state.modalVisible}>
            <View style={styles.modalView}>
              <ScrollView>
                <Text style={{fontSize: 16}}>{this.state.currentModal}</Text>
              </ScrollView>
              <TouchableOpacity style={styles.modalViewButton} onPress={()=>this.showModal()}>
                <Text style={{fontSize: 17, fontWeight: 'bold'}}>Close</Text>
              </TouchableOpacity>
            </View>
          </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    formStates: getFormValues('test')(state)
  };
};

export default connect(
  mapStateToProps,
)(NoteScreen)

