import { StyleSheet, Dimensions } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'
const {width, height} = Dimensions.get('window')
export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  section: {
    width: width,
    height: height,
    alignItems:'center',

  },
noteView1:{
  width: width,
  height: height* 0.35,
  backgroundColor:'blue',
  marginTop: height* 0.02
},
  noteView2:{
    width: width,
    height: height* 0.04,
    backgroundColor:'grey',
    marginTop: height* 0.02
  },
  modalView:{
    width: width * 0.7,
    height: height* 0.5,
    backgroundColor: 'grey',
    marginLeft: width*0.1
  },
  modalViewButton: {
    width: width * 0.7,
    height: height* 0.05,
    backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerView:{
    width: width,
    height: height* 0.07,
  }
})
