import { StyleSheet, Dimensions } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'
const {width, height} = Dimensions.get('window')
export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  section: {
    width: width,
    height: height,
    alignItems:'center',

  },
  inputField:{
    width: width* 0.7,
    height: height*0.07,
    marginLeft: width * 0.15

  },
  btnText:{
    fontSize:18,
    color:'white'
  },
  btn:{
    width: width* 0.7,
    height: height*0.07,
    borderRadius:7,
    marginLeft: width* 0.15,
    marginTop:width* 0.5,
    backgroundColor:'red',
    justifyContent:'center',
    alignItems:'center'
  },
})
